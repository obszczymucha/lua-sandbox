# LUA sandbox

## Setting up
On Arch, install dependencies:
```
sudo pacman -Syy lua luarocks
sudo luarocks install luaunit
```

## Running test watcher
```
./test.sh listen
```

