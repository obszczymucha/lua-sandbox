local M = {}

function M.trim( s )
  return s:gsub( "^%s*(.-)%s*$", "%1" ):gsub( "\n%s+", "\n")
end

function M.trim4( s )
  return s:match "^%s*(.*)":match "(.-)%s*$"
end

return M
