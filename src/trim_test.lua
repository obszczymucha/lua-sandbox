local lu = require( "luaunit" )
local trim = require( "trim" ).trim

function test_should_trim_whitespace()
  local input = "  abc    "
  local result = trim( input )
  lu.assertEquals( result, "abc" )
end

function test_should_trim_multiline_whitespace()
  local input = [[
  #showtooltip
  /cast Princess Kenny
  ]]

  local result = trim( input )

  lu.assertEquals( result, "#showtooltip\n/cast Princess Kenny" )
end

local runner = lu.LuaUnit.new()
runner:setOutputType( "text" )
os.exit( runner:runSuite() )
